Workshop online at: [https://o11y-workshops.gitlab.io/workshop-opentelemetry](https://o11y-workshops.gitlab.io/workshop-opentelemetry)

![Cover Slide](cover.png)


# Running Locally 

`npm install`
`npm build` -> visit localhost:8000
